package ESMreader

import ESMreader.regions.*
import java.io.InvalidClassException

object RegionFactory {
    fun build(inputClass: Class<Region>): Region{
        return when(inputClass){
            IdentificationRegion::class.java -> IdentificationRegion()
            CardIccIdentificationRegion::class.java -> CardIccIdentificationRegion()
            CardChipIdentificationRegion::class.java -> CardChipIdentificationRegion()
            DriverCardApplicationIdentificationRegion::class.java -> DriverCardApplicationIdentificationRegion()
            CardCertificateRegion::class.java -> CardCertificateRegion()
            CACertificateRegion::class.java -> CACertificateRegion()
            CardDrivingLicenceInformationRegion::class.java -> CardDrivingLicenceInformationRegion()
            CardDownloadRegion::class.java -> CardDownloadRegion()
            CardCurrentUseRegion::class.java -> CardCurrentUseRegion()
            CardControlActivityDataRecordRegion::class.java -> CardControlActivityDataRecordRegion()
            else -> throw InvalidClassException("Wrong class type: $inputClass(HOW YOU GET IT?!?!?!?!?!)")
        }
    }
}
