package ESMreader.regions

import ESMreader.regions.types.SessionOpenVehicleRegion
import ESMreader.regions.types.TimeRealRegion

class CardCurrentUseRegion: Region() {
    var sessionOpenTime = TimeRealRegion()
    var sessionOpenVehicle = SessionOpenVehicleRegion()

    init {
        blockType = "CardCurrentUseRegion"
        blockIdentifier = "0507"
    }

    override fun processInternal(byteInput: ByteArray, position: Int): Int {
        var cursor = position + 5
        cursor = sessionOpenTime.process(byteInput, cursor)
        cursor = sessionOpenVehicle.process(byteInput, cursor)
        return cursor
    }

    override fun getShortString(): String = toString()
    override fun toString(): String {
        return "CardCurrentUseRegion(sessionOpenTime=$sessionOpenTime, sessionOpenVehicle=$sessionOpenVehicle)"
    }
}
