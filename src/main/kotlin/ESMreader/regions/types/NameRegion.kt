package ESMreader.regions.types

import ESMreader.regions.Region

class NameRegion: Region() {
    var codePage = UInt8Region()
    var name = StringRegion(35)

    override fun processInternal(byteInput: ByteArray, position: Int): Int {
        var cursor = position

        cursor = codePage.processInternal(byteInput, cursor)
        cursor = name.processInternal(byteInput, cursor)

        return cursor
    }

    override fun getShortString(): String = "\"$name\""
    override fun toString(): String {
        return "(codePage=$codePage, name=$name)"
    }
}