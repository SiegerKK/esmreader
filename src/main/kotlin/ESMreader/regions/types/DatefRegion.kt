package ESMreader.regions.types

import ESMreader.regions.Region
import ESMreader.regions.types.StringRegion

class DatefRegion: Region() {
    var year = BCDStringRegion(2)
    var month = BCDStringRegion(1)
    var day = BCDStringRegion(1)

    override fun processInternal(byteInput: ByteArray, position: Int): Int {
        var cursor = position

        cursor = year.processInternal(byteInput, cursor)
        cursor = month.processInternal(byteInput, cursor)
        cursor = day.processInternal(byteInput, cursor)

        return cursor
    }

    override fun getShortString(): String = "${day.getShortString()}.${month.getShortString()}.${year.getShortString()}"

}