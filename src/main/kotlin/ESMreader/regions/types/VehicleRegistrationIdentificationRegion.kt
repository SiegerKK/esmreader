package ESMreader.regions.types

import ESMreader.regions.Region

class VehicleRegistrationIdentificationRegion: Region() {
    var vehicleRegistrationNation = NationNumericRegion()
    var vehicleRegisstrationNumber = VehicleRegistrationNumberRegion()
    override fun processInternal(byteInput: ByteArray, position: Int): Int {
        var cursor = position
        cursor = vehicleRegistrationNation.process(byteInput, cursor)
        cursor = vehicleRegisstrationNumber.process(byteInput, cursor)
        return cursor
    }

    override fun getShortString(): String = toString()
    override fun toString(): String {
        return "vehicleRegistrationNation=$vehicleRegistrationNation, vehicleRegisstrationNumber=$vehicleRegisstrationNumber"
    }

}

