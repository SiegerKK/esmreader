package ESMreader.regions.types

import ESMreader.regions.Region

class FullCardNumberRegion: Region() {
    var cardType = EquipmentTypeRegion()
    var cardIssuingMemberState = NationNumericRegion()
    var cardNumber = CardNumberRegion()
    override fun processInternal(byteInput: ByteArray, position: Int): Int {
        var cursor = position
        cursor = cardType.process(byteInput, cursor)
        cursor = cardIssuingMemberState.process(byteInput, cursor)
        cursor = cardNumber.process(byteInput, cursor)
        return cursor
    }

    override fun getShortString(): String = toString()
    override fun toString(): String {
        return "cardType=$cardType, cardIssuingMemberState=$cardIssuingMemberState, cardNumber=$cardNumber"
    }

}

