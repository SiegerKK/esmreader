package ESMreader.regions.types

import ESMreader.regions.Region

open class UInt8Region: Region() {
    var value: Byte = -1
    override fun processInternal(byteInput: ByteArray, position: Int): Int {
        value = byteInput[position]
        return position + 1
    }

    override fun getShortString(): String = toString()
    override fun toString(): String = value.toString()

    //TODO: getBit(Int)
}