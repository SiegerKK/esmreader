package ESMreader.regions.types

class EquipmentTypeRegion: UInt8Region() {
    fun getEquipmentType(): String {
        when (value.toInt()) {
            0 -> return "Reserved"
            1 -> return "Driver Card"
            2 -> return "Workshop Card"
            3 -> return "Control Card"
            4 -> return "Company Card"
            5 -> return "Manufacturing Card"
            6 -> return "Vehicle Unit"
            7 -> return "Motion Sensor"
            else -> return "RFU"
        }
    }

    override fun getShortString(): String = getEquipmentType()
    override fun toString(): String = getEquipmentType()
}



