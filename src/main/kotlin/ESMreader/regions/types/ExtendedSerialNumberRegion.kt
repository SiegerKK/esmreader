package ESMreader.regions.types

import ESMreader.regions.Region

class ExtendedSerialNumberRegion: Region() {
    var serialNumber = -1
    var month: Byte = -1
    var year: Byte = -1
    var type: Byte = -1
    var manufacturerCode: Byte = -1

    override fun processInternal(byteInput: ByteArray, position: Int): Int {
        serialNumber = (byteInput[position].toInt() shl 24) +
                    (byteInput[position + 1].toInt() shl 16) +
                    (byteInput[position + 2].toInt() shl 8) +
                    byteInput[position + 3]

        val bcdString = BCDStringRegion(2)
        bcdString.processInternal(byteInput, position + 4)
        month = bcdString.getByteValue(0)
        year = bcdString.getByteValue(1)
        type = byteInput[position + 6]
        manufacturerCode = byteInput[position + 7]

        return position + 8
    }

    override fun getShortString(): String = toString()
    override fun toString(): String {
        return "(serialNumber=$serialNumber, M/Y=$month/$year, type=$type, manufacturerCode=$manufacturerCode)"
    }


}