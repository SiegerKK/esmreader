package ESMreader.regions.types

import ESMreader.regions.Region
import utils.toHexString

open class UInt16Region: Region() {
    var value = ByteArray(2)
    override fun processInternal(byteInput: ByteArray, position: Int): Int {
        value = byteInput.copyOfRange(position, position + 2)
        return position + 2
    }

    fun toHexString(): String = value.toHexString()
    fun getNumber(): Int = value[0].toUByte().toInt().shl(8) + value[1].toUByte().toInt()
    override fun toString(): String = getNumber().toString()
    override fun getShortString(): String = toString()
}