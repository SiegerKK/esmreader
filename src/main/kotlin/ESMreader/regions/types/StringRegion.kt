package ESMreader.regions.types

import ESMreader.regions.Region

open class StringRegion(
    val size: Int
): Region() {
    var value = "EMPTY"
    override fun processInternal(byteInput: ByteArray, position: Int): Int {
        value = byteInput.copyOfRange(position, position + size).decodeToString()
        return position + size
    }

    override fun getShortString(): String {
        return toString()
    }
    override fun toString(): String {
        return value
    }
}