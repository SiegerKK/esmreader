package ESMreader.regions.types

import ESMreader.regions.Region

class VehicleRegistrationNumberRegion: Region() {

    var codePage = UInt8Region()
    var vehicleRegNumber = StringRegion(13) //is a VRN encoded using the specified character set
    override fun processInternal(byteInput: ByteArray, position: Int): Int {
        var cursor = position
        cursor = codePage.process(byteInput, cursor)
        cursor = vehicleRegNumber.process(byteInput, cursor)
        return cursor
    }

    override fun getShortString(): String = toString()
    override fun toString(): String {
        return "(codePage=$codePage, vehicleRegNumber=$vehicleRegNumber)"
    }

}

