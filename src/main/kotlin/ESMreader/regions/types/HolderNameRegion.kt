package ESMreader.regions.types

import ESMreader.regions.Region
import ESMreader.regions.types.NameRegion

class HolderNameRegion: Region() {
    var name = NameRegion()
    var surname = NameRegion()

    override fun processInternal(byteInput: ByteArray, position: Int): Int {
        var cursor = position

        cursor = name.processInternal(byteInput, cursor)
        cursor = surname.processInternal(byteInput, cursor)

        return cursor
    }

    override fun getShortString(): String = "${name.getShortString()}_${surname.getShortString()})"
    override fun toString(): String {
        return "(name=$name, surname=$surname)"
    }
}
