package ESMreader.regions.types

import ESMreader.regions.Region
import utils.toHexString

open class UInt32Region: Region() {
    var value = ByteArray(4)
    override fun processInternal(byteInput: ByteArray, position: Int): Int {
        value = byteInput.copyOfRange(position, position + 4)
        return position + 4
    }

    fun toHexString(): String = value.toHexString()
    fun getNumber(): Int = value[0].toUByte().toInt().shl(8) + value[1].toUByte().toInt()
    override fun toString(): String = getNumber().toString()
    override fun getShortString(): String = toString()
}