package ESMreader.regions.types

import java.sql.Timestamp
import java.util.*


class TimeRealRegion: UInt32Region() {
    fun getTimestamp(): Long{
        return ((value[0].toUByte().toUInt() shl 24) + (value[1].toUByte().toUInt() shl 16) + (value[2].toUByte().toUInt() shl 8) + value[3].toUByte().toUInt()).toLong()
    }

    override fun getShortString(): String = toString()
    override fun toString(): String {
        val timestamp = getTimestamp()
        val stamp = Timestamp(timestamp * 1000)
        val date = Date(stamp.time)
        return date.toString()
    }
}