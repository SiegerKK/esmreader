package ESMreader.regions.types

import ESMreader.regions.Region

class SessionOpenVehicleRegion: Region() {

    var vehicleRegistrationNation = NationNumericRegion()
    var vehicleRegistrationNumber = VehicleRegistrationNumberRegion()

    override fun processInternal(byteInput: ByteArray, position: Int): Int {
        var cursor = position
        cursor = vehicleRegistrationNation.process(byteInput, cursor)
        cursor = vehicleRegistrationNumber.process(byteInput, cursor)
        return cursor
    }

    override fun getShortString(): String = toString()
    override fun toString(): String {
        return "(vehicleRegistrationNation=$vehicleRegistrationNation, vehicleRegistrationNumber=$vehicleRegistrationNumber)"
    }


}
