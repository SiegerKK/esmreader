package ESMreader.regions.types

import kotlin.experimental.and

open class BCDStringRegion(
    var byteSize: Int
): StringRegion(byteSize * 2) {
    override fun processInternal(byteInput: ByteArray, position: Int): Int {
        val frontMask = 0xF0.toByte()
        val backMask = 0x0F.toByte()

        value = ""
        val octet = byteInput.copyOfRange(position, position + byteSize)
        for (byteValue in octet){
            val front = byteValue.and(frontMask).toInt().shr(4).toString()
            val back = byteValue.and(backMask).toInt().toString()
            value += front + back
        }

        return position + byteSize
    }

    fun getByteValue(byteIndex: Int): Byte = value.substring(byteIndex * 2, (byteIndex + 1) * 2).toByte()
}