package ESMreader.regions.types

import ESMreader.regions.Region

class CardNumberRegion: Region() {
    var driverIdentification = StringRegion(14)
    var cardReplacmentIndex = UInt8Region()
    var cardRenewalIndex = UInt8Region()

    override fun processInternal(byteInput: ByteArray, position: Int): Int {
        var cursor = position

        cursor = driverIdentification.processInternal(byteInput, cursor)
        cursor = cardReplacmentIndex.processInternal(byteInput, cursor)
        cursor = cardRenewalIndex.processInternal(byteInput, cursor)

        return cursor
    }

    override fun getShortString(): String = toString()
    override fun toString(): String {
        return "(driverIdentification=$driverIdentification, cardReplacmentIndex=$cardReplacmentIndex, cardRenewalIndex=$cardRenewalIndex)"
    }
}
