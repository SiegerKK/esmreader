package ESMreader.regions

import ESMreader.regions.types.DatefRegion
import ESMreader.regions.types.LanguageRegion
import ESMreader.regions.types.HolderNameRegion


class DriverCardHolderIdentificationRegion: Region() {
    var name = HolderNameRegion()
    var birthDate = DatefRegion()
    var language = LanguageRegion()

    override fun processInternal(byteInput: ByteArray, position: Int): Int {
        var cursor = position

        cursor = name.processInternal(byteInput, cursor)
        cursor = birthDate.processInternal(byteInput, cursor)
        cursor = language.processInternal(byteInput, cursor)

        return cursor
    }

    override fun getShortString(): String = "(name=${name.getShortString()}, birthDate=${birthDate.getShortString()}, language=${language.getShortString()})"
    override fun toString(): String {
        return "(name=$name, birthDate=$birthDate, language=$language)"
    }
}
