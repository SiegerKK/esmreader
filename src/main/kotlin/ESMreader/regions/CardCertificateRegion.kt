package ESMreader.regions

import ESMreader.regions.types.StringRegion

class CardCertificateRegion: Region() {
    var certificate = StringRegion(194) //for Generation 2 size of String = 204..341

    init {
        blockType = "CardCertificate"
        blockIdentifier = "C100"
    }

    override fun processInternal(byteInput: ByteArray, position: Int): Int {
        var cursor = position + 5
        cursor = certificate.processInternal(byteInput, cursor)
        return cursor
    }

    override fun getShortString(): String = toString()
    override fun toString(): String {
        return "(Certificate=$certificate)"
    }

}