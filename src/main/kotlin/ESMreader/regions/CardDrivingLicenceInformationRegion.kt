package ESMreader.regions

import ESMreader.regions.types.NameRegion
import ESMreader.regions.types.NationNumericRegion
import ESMreader.regions.types.StringRegion

class CardDrivingLicenceInformationRegion: Region() {
    var drivingLicenceIssuingAuthority = NameRegion()
    var drivingLicenceIssuingNation = NationNumericRegion()
    var drivingLicenceNumber = StringRegion(16)

    init {
//        blockType = "DrivingLicenceInfo"
        blockType = "CardDrivingLicenceInformation"
        blockIdentifier = "0521"
    }
    override fun processInternal(byteInput: ByteArray, position: Int): Int {
        var cursor = position + 5
        cursor = drivingLicenceIssuingAuthority.process(byteInput, cursor)
        cursor = drivingLicenceIssuingNation.process(byteInput, cursor)
        cursor = drivingLicenceNumber.process(byteInput, cursor)
        return cursor
    }

    override fun getShortString(): String = toString()
    override fun toString(): String {
        return "(drivingLicenceIssuingAuthority=$drivingLicenceIssuingAuthority, drivingLicenceIssuingNation=$drivingLicenceIssuingNation, drivingLicenceNumber=$drivingLicenceNumber)"
    }

}





