package ESMreader.regions

import ESMreader.regions.types.ExtendedSerialNumberRegion
import ESMreader.regions.types.StringRegion
import ESMreader.regions.types.UInt16Region
import ESMreader.regions.types.UInt8Region

//Information, stored in a card, related to the identification of the integrated circuit (IC) card (Annex 1C requirement 248).

class CardIccIdentificationRegion : Region() {
    var clockStop = UInt8Region()
    var cardExtendedSerialNumber = ExtendedSerialNumberRegion()
    var cardApprovalNumber = StringRegion(8)
    var cardPersonaliserId = UInt8Region()
    var countryCode = StringRegion(2)
    var moduleEmbedder = UInt16Region()
    var manufacturerInformation = UInt8Region()
    var icIdentifier = UInt16Region()

    private val regionsStructure = ArrayList<Region>()
    init {
        blockType = "CardIccIdentification"
        blockIdentifier = "0002"

        regionsStructure.add(clockStop)
        regionsStructure.add(cardExtendedSerialNumber)
        regionsStructure.add(cardApprovalNumber)
        regionsStructure.add(cardPersonaliserId)
        regionsStructure.add(countryCode)
        regionsStructure.add(moduleEmbedder)
        regionsStructure.add(manufacturerInformation)
        regionsStructure.add(icIdentifier)
    }

    override fun processInternal(byteInput: ByteArray, position: Int): Int {
        var cursor = position + 5
        for (region in regionsStructure)
            cursor = region.processInternal(byteInput, cursor)
        return cursor
    }

    override fun getShortString(): String = "(" +
            "cardExtendedSerialNumber=${cardExtendedSerialNumber.getShortString()}, " +
            "cardApprovalNumber=$cardApprovalNumber, " +
            "cardPersonaliserId=$cardPersonaliserId, " +
            "countryCode=$countryCode)"

    override fun toString(): String {
        val resultString = StringBuilder("")
        resultString.append("CS:   $clockStop\n" +
                "SN:  |${cardExtendedSerialNumber}|\n" +
                "AN:  |$cardApprovalNumber|\n" +
                "ID:   $cardPersonaliserId\n" +
                "CTR: |$countryCode|\n" +
                "ME:  |${moduleEmbedder.toHexString()}|\n" +
                "MI:   $manufacturerInformation\n" +
                "ICID:|${icIdentifier.toHexString()}|\n")
        return resultString.toString()
    }
}
