package ESMreader.regions

import utils.toHexString

@Deprecated("NOT IMPLEMENTED YET")
class GNSSContinuousDrivingRegion: Region() {
    override fun processInternal(byteInput: ByteArray, position: Int): Int {
        if(byteInput.copyOfRange(position, position + 2).toHexString() == "0524")
            println(byteInput.copyOfRange(position, position + 10).toHexString().uppercase())
        return 0
    }

    override fun getShortString(): String {
        TODO("Not yet implemented")
    }

    override fun toString(): String {
        return "}-{"
    }
}