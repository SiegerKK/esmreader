package ESMreader.regions

import ESMreader.regions.types.CardNumberRegion
import ESMreader.regions.types.NameRegion
import ESMreader.regions.types.NationNumericRegion
import ESMreader.regions.types.TimeRealRegion

class IdentificationRegion: Region() {
    var cardIssuingMemberState = NationNumericRegion()
    var cardNumber = CardNumberRegion()
    var cardIssuingAuthorityName = NameRegion()
    var cardIssueDate = TimeRealRegion()
    var cardValidityBegin = TimeRealRegion()
    var cardExpiryDate = TimeRealRegion()
    var driverCardHolder = DriverCardHolderIdentificationRegion()

    init {
        blockType = "Identification"
        blockIdentifier = "0520"
    }

    override fun processInternal(byteInput: ByteArray, position: Int): Int {
        var cursor = position + 5

        cursor = cardIssuingMemberState.processInternal(byteInput, cursor)
        cursor = cardNumber.processInternal(byteInput, cursor)
        cursor = cardIssuingAuthorityName.processInternal(byteInput, cursor)
        cursor = cardIssueDate.processInternal(byteInput, cursor)
        cursor = cardValidityBegin.processInternal(byteInput, cursor)
        cursor = cardExpiryDate.processInternal(byteInput, cursor)
        cursor = driverCardHolder.processInternal(byteInput, cursor)

        return cursor
    }

    override fun getShortString(): String = "(" +
            "cardIssuingMemberState=${cardIssuingMemberState.getShortString()}, " +
            "cardNumber=${cardNumber.getShortString()}, " +
            "cardIssuingAuthorityName=${cardIssuingAuthorityName.getShortString()}, " +
            "cardIssueDate=${cardIssueDate.getShortString()}, " +
            "cardValidityBegin=${cardValidityBegin.getShortString()}, " +
            "cardExpiryDate=${cardExpiryDate.getShortString()}, " +
            "driverCardHolder=${driverCardHolder.getShortString()})"

    override fun toString(): String {
        return "(cardIssuingMemberState=$cardIssuingMemberState, cardNumber=$cardNumber, cardIssuingAuthorityName=$cardIssuingAuthorityName, cardIssueDate=$cardIssueDate, cardValidityBegin=$cardValidityBegin, cardExpiryDate=$cardExpiryDate, driverCardHolder=$driverCardHolder)"
    }
}
