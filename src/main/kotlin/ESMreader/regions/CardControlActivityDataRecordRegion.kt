package ESMreader.regions

import ESMreader.regions.types.*

class CardControlActivityDataRecordRegion: Region() {
    var controlType = UInt8Region()
    var controlTime = TimeRealRegion()
    var controlCardNumber = FullCardNumberRegion()
    var controlVehicleRegistration = VehicleRegistrationIdentificationRegion()
    var controlDownloadPeriodBegin = TimeRealRegion()
    var controlDownloadPeriodEnd = TimeRealRegion()

    init {
        blockType = "CardControlActivityDataRecord"
        blockIdentifier = "0508"
    }



    override fun processInternal(byteInput: ByteArray, position: Int): Int {
        var cursor = position + 5
        cursor = controlType.process(byteInput, cursor)
        cursor = controlTime.process(byteInput, cursor)
        cursor = controlCardNumber.process(byteInput, cursor)
        cursor = controlVehicleRegistration.process(byteInput, cursor)
        cursor = controlDownloadPeriodBegin.process(byteInput, cursor)
        cursor = controlDownloadPeriodEnd.process(byteInput, cursor)
        return cursor
    }

    override fun getShortString(): String = toString()
    override fun toString(): String {
        return "CardControlActivityDataRecordRegion(controlType=$controlType, controlTime=$controlTime, controlCardNumber=$controlCardNumber, controlVehicleRegistration=$controlVehicleRegistration, controlDownloadPeriodBegin=$controlDownloadPeriodBegin, controlDownloadPeriodEnd=$controlDownloadPeriodEnd)"
    }

}

