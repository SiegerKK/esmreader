package ESMreader.regions

import ESMreader.regions.types.*

class DriverCardApplicationIdentificationRegion: Region() {
    var typeOfTachografCardId = EquipmentTypeRegion()
    var cardStructureVersion = UInt16Region()
    var noOfEventsPerType = UInt8Region()
    var noOfFaultsPerType = UInt8Region()
    var activityStructureLength = UInt16Region()
    var noOfCardVehicleRecords = UInt16Region()
    var noOfCardPlaceRecords = UInt8Region()

    init {
        blockType = "DriverCardApplicationIdentification"
        blockIdentifier = "0501"
    }
    override fun processInternal(byteInput: ByteArray, position: Int): Int {
        var cursor = position + 5

        cursor = typeOfTachografCardId.processInternal(byteInput, cursor)
        cursor = cardStructureVersion.processInternal(byteInput, cursor)
        cursor = noOfEventsPerType.processInternal(byteInput, cursor)
        cursor = noOfFaultsPerType.processInternal(byteInput, cursor)
        cursor = activityStructureLength.processInternal(byteInput, cursor)
        cursor = noOfCardVehicleRecords.processInternal(byteInput, cursor)
        cursor = noOfCardPlaceRecords.processInternal(byteInput, cursor)

        return cursor
    }

    override fun getShortString(): String = toString()
    override fun toString(): String {
        return "(typeOfTachografCardId=$typeOfTachografCardId, cardStructureVersion=$cardStructureVersion, noOfEventsPerType=$noOfEventsPerType, noOfFaultsPerType=$noOfFaultsPerType, activityStructureLength=$activityStructureLength, noOfCardVehicleRecords=$noOfCardVehicleRecords, noOfCardPlaceRecords=$noOfCardPlaceRecords)"
    }
}