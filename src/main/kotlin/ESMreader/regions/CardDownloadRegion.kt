package ESMreader.regions

import ESMreader.regions.types.TimeRealRegion

class CardDownloadRegion: Region() {

    var lastCardDownload = TimeRealRegion()

    init {
        blockType = "CardDownload"
        blockIdentifier = "050E"
    }

    override fun processInternal(byteInput: ByteArray, position: Int): Int {
        var cursor = position + 5
        cursor = lastCardDownload.process(byteInput, cursor)
        return cursor
    }

    override fun getShortString(): String = toString()
    override fun toString(): String {
        return "(lastCardDownload=$lastCardDownload)"
    }
}
