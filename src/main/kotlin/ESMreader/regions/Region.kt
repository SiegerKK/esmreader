package ESMreader.regions

import utils.toHexString

abstract class Region {
    var blockType = "EMPTY BLOCK TYPE"
    var blockIdentifier = "EMPTY BLOCK IDENTIFIER"
    var start = -1
    var length = -1

    fun checkIdentifier(byteInput: ByteArray, position: Int): Boolean{
        val temp = byteInput.copyOfRange(position, position + 2).toHexString().uppercase()
        return temp == blockIdentifier
    }

    fun process(byteInput: ByteArray, position: Int): Int{
        start = position

        return try {
            val end = processInternal(byteInput, position)
            length = end - start
            end
        } catch (ex: Exception){
            ex.printStackTrace()
            start + 2
        }

    }
    // Return index of first byte of new region
    abstract fun processInternal(byteInput: ByteArray, position: Int): Int

    fun getString(): String {
        return "$blockType [0x${Integer.toHexString(start)}-0x${Integer.toHexString(start + length)}, $length] ${getShortString()}"
    }
    abstract fun getShortString(): String
}