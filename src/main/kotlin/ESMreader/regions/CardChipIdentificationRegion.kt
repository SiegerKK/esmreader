package ESMreader.regions

import ESMreader.regions.types.StringRegion
import ESMreader.regions.types.UInt32Region

class CardChipIdentificationRegion: Region() {
//    var icSerialNumber = StringRegion(4)
//    var icManufacturingReferences = StringRegion(4)
    var icSerialNumber = UInt32Region()
    var icManufacturingReferences = UInt32Region()

    init {
        blockType = "CardChipIdentification"
        blockIdentifier = "0005"
    }

    override fun processInternal(byteInput: ByteArray, position: Int): Int {
        var cursor = position + 5

        cursor = icSerialNumber.processInternal(byteInput, cursor)
        cursor = icManufacturingReferences.processInternal(byteInput, cursor)

        return cursor
    }

    override fun getShortString(): String = toString()
    override fun toString(): String {
        return "(icSerialNumber=${icSerialNumber.toHexString()}, icManufacturingReferences=${icManufacturingReferences.toHexString()})"
    }
}