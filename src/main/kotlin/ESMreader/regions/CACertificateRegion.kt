package ESMreader.regions

class CACertificateRegion: Region() {
    var memberStateCertificate = CardCertificateRegion().certificate
    override fun processInternal(byteInput: ByteArray, position: Int): Int {
        var cursor = position + 5
        cursor = memberStateCertificate.processInternal(byteInput, cursor)
        return cursor
    }

    init{
        blockType = "CACertificate"
        blockIdentifier = "C108"
    }

    override fun getShortString(): String = toString()
    override fun toString(): String {
        return "(memberStateCertificate=$memberStateCertificate)"
    }

}
