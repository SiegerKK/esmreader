package ESMreader

import ESMreader.regions.*
import utils.FileManager

object ESMReader {
    val templates = ArrayList<Region>()
    init {
//        templates.add(CardIccIdentificationRegion())
//        templates.add(IdentificationRegion())
//        templates.add(CardChipIdentificationRegion())
//        templates.add(DriverCardApplicationIdentificationRegion())
//        templates.add(CardCertificateRegion())
//        templates.add(CACertificateRegion())
//        templates.add(CardDrivingLicenceInformationRegion())
//        templates.add(CardDownloadRegion())
//        templates.add(CardCurrentUseRegion())
        templates.add(CardControlActivityDataRecordRegion())
    }

    fun processFile(filename: String): ESMObject{
        val binaryFile = FileManager.readBytes(filename)
        val resultESMObject = ESMObject()

        var position = 0
        while (position < binaryFile.size - 4){
            for (template in templates){
                if(template.checkIdentifier(binaryFile, position)) {
                    // template.process() return FIRST byte of NEW block
                    val newRegion = RegionFactory.build(template.javaClass)
                    position = newRegion.process(binaryFile, position) - 1
                    resultESMObject.regions.add(newRegion)
                    break
                }
            }
            position++
        }
        return resultESMObject
    }
}
