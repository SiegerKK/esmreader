import ESMreader.ESMReader
import utils.FileManager

object Main{
    @JvmStatic
    fun main(args: Array<String>){
        val baseAddress = "src/main/resources/driver/"
        for (filename in FileManager.getFilesInFolder(baseAddress)){
            println("-------FILE $filename")
            val esmObject = ESMReader.processFile("src/main/resources/driver/$filename")
            for (region in esmObject.regions)
                println(region.getString())
            println("-------END OF FILE $filename")
        }

    }

//    File("/tmp").walkTopDown().forEach { println(it) }


}
