package utils

import java.io.*
import java.net.URL

object FileManager {
    private const val TELEGRAM_ACCOUNTS = "telegramUsers.json"
    private const val TELEGRAM_TOKEN = "botToken.json"

    fun readBytes(fileName: String): ByteArray{
        val resultBytes: ByteArray = File(fileName).readBytes()
        return resultBytes
    }
    fun readFile(url: URL): String {
        val reader: BufferedReader
        val file = StringBuffer()
        try {
            reader = BufferedReader(InputStreamReader(url.openStream()))
            var line = reader.readLine()
            while (line != null) {
                file.append(line.trimIndent())
                line = reader.readLine()
            }
            reader.close()
        } catch (ex: IOException) {
            ex.printStackTrace()
        }
        return file.toString()
    }
    fun readFile(filename: String): String {
        val reader: BufferedReader
        val file = StringBuffer()
        try {
            reader = BufferedReader(FileReader(filename))
            var line = reader.readLine()
            while (line != null) {
                file.append(line.trimIndent())
                line = reader.readLine()
            }
            reader.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return file.toString()
    }
    fun writeToFile(str: Any, url: URL) {
        val writer: BufferedWriter
        try {
            val connection = url.openConnection()
            connection.doOutput = true
            writer = BufferedWriter(OutputStreamWriter(connection.getOutputStream()))
            writer.write(str.toString())
            writer.close()
        } catch (ex: Exception) {
            println(url)
            ex.printStackTrace()
        }
    }
    fun writeToFile(str: Any, filename: String) {
        try {
            val file = File(filename)
            if (!file.exists()) file.createNewFile()
            val wr: Writer = FileWriter(file, false)
            wr.write(str.toString())
            wr.close()
        } catch (ex: Exception) {
            println(filename)
            ex.printStackTrace()
        }
    }
    fun appendToFile(str: Any, filename: String) {
        try {
            val file = File(filename)
            if (!file.exists()) file.createNewFile()
            val wr: Writer = FileWriter(file, true)
            wr.write(str.toString())
            wr.close()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }
    fun getFilesInFolder(folderAddress: String): List<String> {
        val results: MutableList<String> = ArrayList()
        val files = File(folderAddress).listFiles()
        for (file in files) {
            if (file.isFile) {
                results.add(file.name)
            }
        }
        return results
    }
}

fun ByteArray.toHexString() = joinToString("") { "%02x".format(it) }